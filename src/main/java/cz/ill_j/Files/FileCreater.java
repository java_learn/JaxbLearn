package cz.ill_j.Files;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;

public class FileCreater {

	private String path = System.getenv("APPDATA") + File.separator + "LearnJava";

	/////////////
	/*
	 * FileWriter for testing only
	 */
	private boolean writeXmlToFile(String epXml) {

		File fSlozka = new File(path);
		if (!fSlozka.isDirectory()) {
			fSlozka.mkdir();
		}

		File fSoubor = new File(path + File.separator + "EP_XML" + ".xml");
		try {
			if (!fSoubor.exists()) {
				fSoubor.createNewFile();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		// zapis do souboru
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(fSoubor, true));
			bw.write(epXml);
			bw.newLine();
			bw.flush();
			// System.out.println("Zapis se zdaril");
			bw.close();
		} catch (Exception e) {
			System.err.println("Do souboru se nepovedlo zapsat.");
		}

		return true;
	}

	/**
	 * 
	 * @param fileName
	 * @return overloaded method return statement
	 */
	public FileOutputStream getOutputStream(String fileName) {

		return getOutputStream(getFile(fileName));
	}
	
	public FileOutputStream getOutputStream(File file) {
		
		try {
			// 
			return new FileOutputStream(file);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * @param fileName
	 * @return
	 */
	private File getFile(String fileName) {

		File fSlozka = new File(path);
		if (!fSlozka.isDirectory()) {
			fSlozka.mkdir();
		}

		File fSoubor = new File(path + File.separator + fileName);
		try {
			if (!fSoubor.exists()) {
				fSoubor.createNewFile();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return fSoubor;
	}

}
