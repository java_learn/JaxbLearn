package cz.ill_j.Generics;

public class TclassGen<T> {
	
	   private T t;

	   public void add(T t) {
	      this.t = t;
	   }

	   public T get() {
	      return t;
	   }

	   public static void main(String[] args) {
		  TclassGen<Integer> integerBox = new TclassGen<Integer>();
		  TclassGen<String> stringBox = new TclassGen<String>();
	    
	      integerBox.add(new Integer(10));
	      stringBox.add(new String("Hello World"));

	      System.out.printf("Integer Value :%d\n\n", integerBox.get());
	      System.out.printf("String Value :%s\n", stringBox.get());
	   }

}
