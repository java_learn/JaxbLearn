package cz.ill_j.Validation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * {@link RunValidation} contains proces from loading pattern XSD to validation
 * XSD and XML files.
 * 
 * @author ill_j
 *
 */
public class RunValidation {

	private static File xsdFile = new File("src/main/resources/xsd/EP.xsd");

	private static String absolutePath = xsdFile.getAbsolutePath();

	private static final String xsdFilePath = absolutePath;
	
	private static File xmlDir = new File("src/main/resources/epXml/");

	static XmlValidator xmlValidator;

	public static void main(String[] args) {

		try {
			// loading xsd pattern
			InputStream inputStream = new FileInputStream(xsdFilePath);
			System.out.println("Soubor '" + absolutePath + "' jsem nasel.");

			xmlValidator = new XmlValidator(inputStream);

			// validation process
			validateAllXmlFiles();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void validateAllXmlFiles() throws FileNotFoundException {

		for (InputStream xmlFile : loadXmlInputStreamList()) {	
			boolean f = xmlValidator.compareWithXSD(xmlFile);
			System.out.println(f);
		}		
	}

	/**
	 * 
	 * @return List of all files in directory.
	 * @throws FileNotFoundException
	 */
	private static List<InputStream> loadXmlInputStreamList () throws FileNotFoundException {
		
		List<InputStream> xmlInputStream = new ArrayList<InputStream>();

		for (File file : xmlDir.listFiles()) {
			xmlInputStream.add(new FileInputStream(file));
			System.out.println("File '" + file.getName() + "' loaded successfully.");
		}
		
		return xmlInputStream;
	}

}
