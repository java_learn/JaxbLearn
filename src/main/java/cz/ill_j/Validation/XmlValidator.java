package cz.ill_j.Validation;

import java.io.InputStream;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xml.sax.SAXException;

/**
 * {@link XmlValidator} covers other file formats validation methods.  
 * 
 * @author ill_j
 *
 */
public class XmlValidator {
	
	Validator validator;
	
	/**
	 * Constructor
	 * 
	 * @param InputStream xsd
	 */
	public XmlValidator(InputStream xsd) {
		SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.XML_NS_URI);
        Schema schema;
		try {
			schema = factory.newSchema(new StreamSource(xsd));
			validator = schema.newValidator();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
	}

	/**
	 * Compares XML and XSD files.
	 * 
	 * @param xml
	 * @return true if method doesn't throw any exception.<br>
	 * 			It means XML and XSD match.
	 */
	public boolean compareWithXSD(InputStream xml)
	{		
	    try
	    {
	        validator.validate(new StreamSource(xml));
	        
	        return true;
	    }
	    catch(Exception ex)
	    {
	    	ex.printStackTrace();
	        return false;
	    }
	}
}
