package cz.ill_j.inheritanceLearn;

public class Car extends Vehicle {

	public Car() {
		super();
	}

	/**
	 * call super method
	 */
	public void carOptions() {
		super.engineType(EngineType.DIESEL.toString());
	}

	/**
	 * override super method
	 */
	@Override
	public void engineType(String engineType) {
		super.engineType(engineType);
		System.out.println("@Override engineType()_");
	}
	
	public String getParentString () {
		return engineNoise;
	}
	
	public void setParentString (String engineNoise) {
		this.engineNoise = engineNoise;
	} 
}
