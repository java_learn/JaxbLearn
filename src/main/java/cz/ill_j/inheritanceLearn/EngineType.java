package cz.ill_j.inheritanceLearn;

public enum EngineType {

	DIESEL,
	
	GASOLINE,
	
	ELECTRIC;
}
