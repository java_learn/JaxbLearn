package cz.ill_j.inheritanceLearn;

public class StartVehicles {

	public static void main(String[] args) {

		Car car = new Car();
		car.carOptions();
		car.engineType(EngineType.ELECTRIC.toString());
		
		car.setParentString("Vruuum");
		System.out.println(car.getParentString());
	}

}
