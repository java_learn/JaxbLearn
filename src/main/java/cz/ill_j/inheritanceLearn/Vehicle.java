package cz.ill_j.inheritanceLearn;

public class Vehicle {

	public String engineNoise;
	
	public Vehicle () {
		System.out.println("I'm a vehicle class constructor.");
	}
	
	public void engineType (String engineType) {
		System.out.println("I have " + engineType + " engine. Printed from Vehicle class.");
	}
}
