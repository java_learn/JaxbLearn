package cz.ill_j.makro;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

public class CreateTestFiles {

	private static final String IN_FOLDER = "c:/TMP/makroFileSystem/CZ/IN/";
	private static final String OUT_FOLDER = "c:/TMP/makroFileSystem/CZ/OUT/";
	private static final String TRASH_FOLDER = "c:/TMP/makroFileSystem/CZ/_TRASH/";

	private static final String TEST_IN_FOLDER = "c:/TMP/makroFileSystem/CZ/TEST/";
    
    private static final String DATE_TIME_FORMAT = "yyyyMMddHHmmss";
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT);
    
    private static final String UNDERSCORE = "_";
    private static final String FILE_SEPARATOR = "/";
	private static final String YEAR = "2018";
	private static final String EXTENSION = ".zip";
	
	private static String[] fileTypes = {"ATP", "ARTICLE"}; 
	
	static Random r = new Random();
	static int monthLow = 1;
	static int monthHigh = 9;
	static int dayLow = 10;
	static int dayHigh = 28;
	static int hourLow = 10;
	static int hourHigh = 23;
	static int minLow = 10;
	static int minHigh = 60;
	static int secLow = 10;
	static int secHigh = 60;

	public static void main(String[] args) {
		System.out.println("Start");
		for (String fileType : fileTypes) {
			for (int version = 1; version < 5; version++ ) {
				for (int i = 0; i < 97; i++ ) {
					String loadType = "DELTA";
					if (i%5 == 0) {
						loadType = "FULL";
					}
					System.out.println(createFile(fileType, Integer.toString(version), loadType));
				}
			}
		}
		System.out.println("Finito");
	}

	private static String createFile(String fileType, String version, String loadType) {
		Path filePath = Paths.get(IN_FOLDER, fileType, version, getFileName(fileType, version, loadType));
        // creates non-existing directories
        try {
            Files.createDirectories(filePath.getParent());
        } catch (IOException e) {
			e.printStackTrace();
        }
		try {
			Files.createFile(filePath);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return filePath.toString();
	}

	private static String getFileName(String fileType, String version, String loadType) {
		return fileType + UNDERSCORE + version + UNDERSCORE + loadType + UNDERSCORE + getRandomLocalDateTime().format(FORMATTER) + EXTENSION;
	}

	private static LocalDateTime getRandomLocalDateTime() {
		String date = YEAR + "0" 
			+ Integer.toString(r.nextInt(monthHigh-monthLow)+monthLow)
			+ Integer.toString(r.nextInt(dayHigh-dayLow)+dayLow)
			+ Integer.toString(r.nextInt(hourHigh-hourLow)+hourLow)
			+ Integer.toString(r.nextInt(minHigh-minLow)+minLow)
			+ Integer.toString(r.nextInt(secHigh-secLow)+secLow);
		return LocalDateTime.parse(date, FORMATTER);
	}

}
