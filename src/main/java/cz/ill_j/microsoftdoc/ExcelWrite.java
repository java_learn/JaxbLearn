package cz.ill_j.microsoftdoc;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import cz.ill_j.Files.FileCreater;

public class ExcelWrite {

	private final String FILE_NAME = "MyFirstExcel.xlsx";
	
	public void createExcelSheet() {
		Object[][] datatypes = {
                {"Datatype", "Type", "Size(in bytes)"},
                {"int", "Primitive", 2},
                {"float", "Primitive", 4},
                {"double", "Primitive", 8},
                {"char", "Primitive", 1},
                {"String", "Non-Primitive", "No fixed size"}
        };
		createExcelSheet("Disc", datatypes);
	}
	
	public void createExcelSheet (String sheetName, Object[][] sheetContent) {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet(sheetName);
        
        int rowNum = 0;
        System.out.println("Creating excel");

        for (Object[] dataRow : sheetContent) {
            XSSFRow row = sheet.createRow(rowNum++);
            int colNum = 0;
            for (Object field : dataRow) {
                XSSFCell cell = row.createCell(colNum++);
                if (field instanceof String) {
                    cell.setCellValue((String) field);
                } else if (field instanceof Integer) {
                    cell.setCellValue((Integer) field);
                }
            }
        }

        try {
        	FileCreater creater = new FileCreater();
            FileOutputStream outputStream = creater.getOutputStream(FILE_NAME);
        	
            workbook.write(outputStream);
            workbook.close();
            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Done");
	}
	
	private Workbook getWorkbook(String excelFilePath) throws IOException {
	    Workbook workbook = null;
	 
	    if (excelFilePath.endsWith("xlsx")) {
	        workbook = new XSSFWorkbook();
	    } else if (excelFilePath.endsWith("xls")) {
	        workbook = new HSSFWorkbook();
	    } else {
	        throw new IllegalArgumentException("The specified file is not Excel file");
	    }
	 
	    return workbook;
	}
	
}
