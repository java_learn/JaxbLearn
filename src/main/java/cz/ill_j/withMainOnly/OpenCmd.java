package cz.ill_j.withMainOnly;

import java.io.IOException;

public class OpenCmd {

	public static void main(String[] args) {
		String serversDir = "Servers";
		
		Runtime rt = Runtime.getRuntime();
		try {
//			rt.exec(new String[]{"cmd.exe","/c cd \"" + serversDir + "\"","start"});
			rt.exec(new String[]{"cmd.exe","/c","start"});
		} catch (IOException e) {
			System.out.println(e);
			e.printStackTrace();
		}
		// c:\Servers\wisor-sdk-1.0.0-SNAPSHOT\jboss-eap-6.4\bin>standalone.bat
	}

}
