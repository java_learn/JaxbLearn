package cz.ill_j.withMainOnly;

import java.util.ArrayList;
import java.util.List;

public class PinoArrayList {

	private List<String> list = new ArrayList<>();
	private List<String> list2;
	private static PinoArrayList object = new PinoArrayList();
	
	public static void main(String[] args) {
		List<String> names = new ArrayList<>();
		names.add("toto");
		names.add("Lala");
		names.add("papa");
		int index = names.indexOf("papa");
		System.out.println("Index papa: " + index);
		
		object.addToList();
		object.list2 = object.getList();
		object.list2.remove(0);
		System.out.println(object.list);
		System.out.println(object.list2);
	}

	void addToList() {
		list.add("Ahoj");
		list.add("Svete");
	}

	public List<String> getList() {
		return list;
	}
}
