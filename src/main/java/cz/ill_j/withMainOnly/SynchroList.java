package cz.ill_j.withMainOnly;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

public class SynchroList {

	private static Random random = new Random();
	
	public static void main(String[] args) {
		final List<String> list = Collections.synchronizedList(new CopyOnWriteArrayList<String>());;

		for (int i = 0; i < 100000; i++) {
			list.add("nazdar" + Integer.toString(random.nextInt(1000)));
		}
		
		Thread t1 = new Thread(new Runnable() {
			
			@Override
			public void run() {
				Iterator<String> iterator = list.iterator();
				int i1 = 0;
				while (iterator.hasNext()) {
					iterator.next();
					if (i1%5 == 0) {
						iterator.remove();
					}
					i1++;
				}
			}
		});
		
		Thread t2 = new Thread(new Runnable() {
			
			@Override
			public void run() {
				for (int i = 0; i < 10000; i++) {
					list.add("hello" + Integer.toString(random.nextInt(1000)));
				}	
			}
		});
		
		Thread t3 = new Thread(new Runnable() {
			
			@Override
			public void run() {
				for (String word : list) {
					System.out.println(word);
					}
				}
		});
		
		try {
			t1.start();
			t2.start();
//			t3.start();
			t1.join();
			t2.join();
			t3.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
